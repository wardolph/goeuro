# webapp

## Running with IDE
The actual project is called webapp which is inside the current GoEuro folder. It was created just so screenshots etc could be added. They are not part of the project. 



## running without IDE

run maven install

mvn clean install

It will generate jar 2 files in target directory. run jar file which includes all dependencies webapp-0.0.1-SNAPSHOT-jar-with-dependencies.jar

It will create a csv file in your current directory with results.

command line running example:

java -jar webapp-0.0.1-SNAPSHOT-jar-with-dependencies.jar Berlin


screen shots in screenshot folder

Testing

Test coverage shows up as less. config files and model objects are not being tested

# Technologies used:


Java 8, 

Spring, 

Jackson, 

Junit, 

hamcrest
