package com.tech.launcher;

import com.tech.configuration.DIConfiguration;
import com.tech.consumer.CitySuggestionConsumer;
import com.tech.consumer.LocationWriterConsumer;
import com.tech.model.Location;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.util.List;

/**
 * Created by Jahangir on 02/08/2016.
 */
public class CitySuggestionLauncher {
	
	private static final Logger logger = Logger.getLogger(CitySuggestionLauncher.class);

	public static void main(String[] args) {
				
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DIConfiguration.class);
		CitySuggestionConsumer app = context.getBean(CitySuggestionConsumer.class);
		if(logger.isDebugEnabled()){
			logger.debug("Getting city suggestions");
		}

		List<Location> locations = null;
		try{
			if(args.length==0 || StringUtils.isBlank(args[0])){
				throw new IllegalArgumentException("Please send a city name as first argument");
			}
			locations = app.getSuggestedLocations(args[0]);//getting results from service
		}
		catch (IOException e){
			logger.error("unable get results from service",e);
		}

		if(logger.isDebugEnabled()){
			logger.debug("Writing results");
		}
		LocationWriterConsumer writer = context.getBean(LocationWriterConsumer.class);

		try{
			writer.outputResults(locations);//writing results to file
		}
		catch (IOException e){
			logger.error("unable to write csv results to file",e);
		}

		if(logger.isDebugEnabled()){
			logger.debug("Done");
		}
		//close the context
		context.close();
	}
	
}
