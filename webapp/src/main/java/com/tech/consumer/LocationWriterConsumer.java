package com.tech.consumer;

import com.tech.model.Location;
import com.tech.service.CitySuggestionService;
import com.tech.service.LocationWriterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * Created by Jahangir on 02/08/2016.
 */
@Component
public class LocationWriterConsumer {

    private LocationWriterService service;

    @Autowired
    public void setService(LocationWriterService svc){
        this.service=svc;
    }

    public void outputResults(List<Location> locations) throws IOException {
        this.service.outputResults(locations);
    }



}
