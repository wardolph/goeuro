package com.tech.consumer;

import com.tech.model.Location;
import com.tech.service.CitySuggestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * Created by Jahangir on 02/08/2016.
 */
@Component
public class CitySuggestionConsumer {

    private CitySuggestionService service;

    @Autowired
    public void setService(CitySuggestionService svc){
        this.service=svc;
    }

    public List<Location> getSuggestedLocations(String name) throws IOException {
        return this.service.getSuggestedLocations(name);
    }



}
