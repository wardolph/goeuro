package com.tech.configuration;

import com.tech.service.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value={"com.tech.consumer"})
public class DIConfiguration {

	@Bean
	public CitySuggestionService getCitySuggestionService(@Value("http://api.goeuro.com/api/v2/position/suggest/en/") String url){
		return new CitySuggestionServiceImpl(url);
	}
	
	@Bean
	public LocationWriterService getLocationWriterService(@Value("LocationResults.csv") String fileName){
		return new LocationWriterServiceCsvImpl(fileName);
	}
}