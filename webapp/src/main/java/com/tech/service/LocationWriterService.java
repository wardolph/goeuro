package com.tech.service;

import com.tech.model.Location;

import java.io.IOException;
import java.util.List;

/**
 * Created by Jahangir on 02/08/2016.
 */
public interface LocationWriterService {

    public void outputResults(List<Location> locations) throws IOException;
}
