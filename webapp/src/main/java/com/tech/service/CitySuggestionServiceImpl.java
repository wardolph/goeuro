package com.tech.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tech.model.Location;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Jahangir on 02/08/2016.
 */
public class CitySuggestionServiceImpl implements CitySuggestionService{

    private String serviceUrl;

    public CitySuggestionServiceImpl(String serviceUrl){

        this.serviceUrl = serviceUrl;
    }

    @Override
    public List<Location> getSuggestedLocations(String name) throws  IOException, IllegalArgumentException{
        if( StringUtils.isBlank(name)){
            throw new IllegalArgumentException("Location name is empty or null. Unable to get suggestions");
        }
        String requestUrl = getRequestUrl(name);
        URL url = new URL(requestUrl);
        ObjectMapper mapper = new ObjectMapper();
        List<Location> locations;
        try {
            locations = mapper.readValue(url, new TypeReference<List<Location>>(){});
        } catch (IOException e) {
            throw new IOException("Unable to get suggestions from service for city: "+name,e);
        }

        return locations;
    }

    private String getRequestUrl(String name){
        String requestUrl = this.serviceUrl + name;

        return requestUrl;
    }
}
