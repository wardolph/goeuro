package com.tech.service;

import com.tech.model.Location;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.io.FileWriter;

/**
 * Created by Jahangir on 02/08/2016.
 */
public class LocationWriterServiceCsvImpl implements LocationWriterService{

    private String outputPath;
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";


    public LocationWriterServiceCsvImpl(String outputPath){
        this.outputPath = outputPath;
    }

    @Override
    public void outputResults(List<Location> locations) throws IOException {

        String fileHeader = "_id,name,type,latitude,longitude";

        PrintWriter writer;
        try {
            writer = new PrintWriter(outputPath, "UTF-8");
            writer.println(fileHeader);

            if(locations!=null) {
                for (Location location : locations) {
                    writer.append(String.valueOf(location.get_id()));
                    writer.append(COMMA_DELIMITER);
                    writer.append(location.getName());
                    writer.append(COMMA_DELIMITER);
                    writer.append(location.getType());
                    writer.append(COMMA_DELIMITER);
                    writer.append(String.valueOf(location.getGeo_position().getLatitude()));
                    writer.append(COMMA_DELIMITER);
                    writer.append(String.valueOf(location.getGeo_position().getLongitude()));
                    writer.append(NEW_LINE_SEPARATOR);
                }
            }

            writer.close();

        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            throw new IOException("FormulaOneJsonFileWriter: Unable to read File "+outputPath, e);
        }
    }
}
