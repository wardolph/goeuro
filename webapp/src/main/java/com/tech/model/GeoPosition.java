package com.tech.model;

/**
 * Created by Jahangir on 02/08/2016.
 */
public class GeoPosition {
    private Double latitude;
    private Double longitude;

    public GeoPosition(){

    }

    public GeoPosition(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
