package com.tech.service;

import com.tech.model.Location;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Jahangir on 02/08/2016.
 */
public class CitySuggestionServiceTest {


    private AnnotationConfigApplicationContext context = null;

    @Bean
    public CitySuggestionService getCitySuggestionService(@Value("http://api.goeuro.com/api/v2/position/suggest/en/") String url) {
        return new CitySuggestionServiceImpl(url);
    }

    @Before
    public void setup() throws Exception {
        context = new AnnotationConfigApplicationContext(CitySuggestionServiceTest.class);
    }

    @After
    public void tearDown() throws Exception {
        context.close();
    }

    @Test
    public void suggestionService_getSuggestions_returnsLocations() throws IOException {
        CitySuggestionService app = context.getBean(CitySuggestionService.class);
        List<Location> locationsActual = app.getSuggestedLocations("Berlin");

        Location location = locationsActual.stream().filter(l -> l.getLocationId().equals(8384)).findFirst().get();
        //test if Berlin 8384 is returned with right name and coordinates.
        Assert.assertNotNull(location);
        Assert.assertTrue(location instanceof Location);
        Assert.assertThat(location.getName(), CoreMatchers.is("Berlin"));
        Assert.assertThat(location.getGeo_position().getLatitude(), CoreMatchers.is(52.52437));
        Assert.assertThat(location.getGeo_position().getLongitude(), CoreMatchers.is(13.41053));

        //can test for other locations as well.
    }

    @Test
    public void suggestionService_getSuggestionsIncorrectName_emptyList() throws IOException {
        CitySuggestionService app = context.getBean(CitySuggestionService.class);
        List<Location> locationsActual = app.getSuggestedLocations("cityNameNotFound");

        Assert.assertThat(locationsActual.isEmpty(), CoreMatchers.is(true));

    }

    @Test(expected = IllegalArgumentException.class)
    public void suggestionServiceNull_getSuggestions_throwsException() throws IllegalArgumentException, IOException {
        CitySuggestionService app = context.getBean(CitySuggestionService.class);
        app.getSuggestedLocations("");//throws exception because url is not correct. Location parameter cannot be empty
    }
}
