package com.tech.service;

import com.tech.model.GeoPosition;
import com.tech.model.Location;
import junit.framework.Assert;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jahangir on 02/08/2016.
 */
public class LocationWriterServiceCsvImplTest {

    private static final String fileName = "locationCSVFileActual.csv";
    private static final String expectedFileName = "locationCSVFileExpected.csv";
    private static String pathToFile;
    private AnnotationConfigApplicationContext context = null;

    @Before
    public void setup(){
        pathToFile = this.getClass().getResource(fileName).getFile();
        context = new AnnotationConfigApplicationContext(LocationWriterServiceCsvImplTest.class);
    }

    @Bean
    public LocationWriterService getLocationWriterService() {
        return new LocationWriterServiceCsvImpl(pathToFile);
    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
        context.close();
    }

    @Test
    public void csvWriterService_outputResults_writesCSV() throws IOException {
        LocationWriterService app = context.getBean(LocationWriterService.class);

        List<Location> locations = new ArrayList<>();

        Location location1 = new Location();
        location1.set_id(376217);
        location1.setName("Berlin");
        location1.setType("location");
        location1.setGeo_position(new GeoPosition(52.52437,13.41053));
        locations.add(location1);

        Location location2 = new Location();
        location2.setType("location");
        location2.setGeo_position(new GeoPosition());
        locations.add(location2);

        Location location3 = new Location();
        location3.set_id(333977);
        location3.setName("Berlin Ostbahnhof");
        location3.setType("station");
        location3.setGeo_position(new GeoPosition(52.510972,13.434567));
        locations.add(location3);

        app.outputResults(locations);

        //Write list to file and check against expected file. Using utf8
        String pathToFileExpected = this.getClass().getResource(expectedFileName).getFile();
        File expectedFile = new File(pathToFileExpected);
        String expected = FileUtils.readFileToString(expectedFile, "utf-8");
        File actualFile = new File(pathToFile);
        String actual = FileUtils.readFileToString(actualFile, "utf-8");
        Assert.assertEquals(expected, actual);
    }
}
